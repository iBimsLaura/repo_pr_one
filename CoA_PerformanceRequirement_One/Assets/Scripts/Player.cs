﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private bool isAlive;

        public bool isAlivePub
        {
            set { isAlive = value; }
            get { return isAlive; }
        }


        [SerializeField]
        private int currentHealth;

        public int currentHealthPub
        {
            set { currentHealth = value; }
            get { return currentHealth; }
        }

        [SerializeField]
        private int attackAmount;

        public int attackAmountPub
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        [SerializeField]
        private Enemy enemyEntity;

        public Enemy enemyEntityPub
        {
            set { enemyEntity = value; }
            get { return enemyEntity; }
        }

        public void AttackEnemy(Enemy enemy, int attackDamage)
        {
            if (enemy.isAlivePub)
            {
                enemy.Damage(attackDamage);
            }
        }

        public void Start()
        {
            Log(entityNamePub);
            Log(currentHealth);
            Log(attackAmount);
            AttackEnemy(enemyEntity, attackAmountPub);
        }
    }
}

