﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public interface ILog
    {
        void Log(object message);
    }
}

