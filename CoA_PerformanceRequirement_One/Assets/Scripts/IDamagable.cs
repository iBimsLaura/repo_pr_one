﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public interface IDamagable
    {
        void Damage(int damageAmount);
    }
}

