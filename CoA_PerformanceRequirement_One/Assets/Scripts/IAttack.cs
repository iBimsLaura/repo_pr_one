﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public interface IAttack
    {
        void AttackEnemy(Enemy enemy, int attackDamage);
    }
}
