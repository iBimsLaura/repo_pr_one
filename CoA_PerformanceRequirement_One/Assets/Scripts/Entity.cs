﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public class Entity : MonoBehaviour, ILog
    {
        [SerializeField]
        private string entityName;

        public string entityNamePub
        {
            set { entityName = value; }
            get { return entityName; }
        }

        public void Log(object message)
        {
            Debug.Log(message);
        }
       
    }
}

