﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public class Enemy : Entity, IDamagable
    {   
        [SerializeField]
        private bool isAlive;

        public bool isAlivePub
        {
            set { isAlive = value; }
            get { return isAlive; }
        }


        [SerializeField]
        private int currentHealth;

        public int currentHealthPub
        {
            set { currentHealth = value; }
            get { return currentHealth; }
        }

        [SerializeField]
        private int attackAmount;

        public int attackAmountPub
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        public void Damage(int damageAmount)
        {
            if(currentHealth > 0)
            {
                currentHealth -= damageAmount;

                if(currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log("The enemy's name is:" + entityNamePub + "their current health is:" + currentHealth);
                    return;
                }
            }

            else
            {
                currentHealth = 0;
                isAlive = false;
                Log("The enemy's name is:" + entityNamePub + "enemy has been damaged" + "their current health is:" + currentHealth);
                return;
            }

            Log("The enemy's name is:" + entityNamePub + "enemy has been damaged" + "their current health is:" + currentHealth);
        }


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
